import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {
    path: 'data',
    loadChildren: () => import('thirdPartyDataIntegrationUi/CompanyData').then(m => m.ThirdPartyDataRecordViewModule)
  },
  {
    path: 'table',
    loadChildren: () => import('thirdPartyDataIntegrationUi/SearchData').then(m => m.ThirdPartyDataTableViewModule)
  },
  {
    path: 'landing',
    loadChildren: () => import('thirdPartyDataIntegrationUi/SearchSources').then(m => m.ThirdPartyDataLandingViewModule)
  },
  {
    path: 'admin/third-party-data/landing', redirectTo: 'landing', pathMatch: 'full'

  },
  {
    path: 'admin/third-party-data/table', redirectTo: 'table', pathMatch: 'full'

  },
  {
    path: 'admin/third-party-data/record/:provider/:id', redirectTo: 'data/:provider/:id', pathMatch: 'full'
  },
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
