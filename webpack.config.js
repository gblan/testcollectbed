const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, 'tsconfig.json'),
  [/* mapped paths to share */]);

module.exports = function(remoteEndpoint = "https://dev-tpdi.kingland-cognitive.com/ui/remoteEntry.js") {
  return {
    output: {
      uniqueName: "testCollectBed",
      publicPath: "auto"
    },
    optimization: {
      runtimeChunk: false
    },
    resolve: {
      alias: {
        ...sharedMappings.getAliases(),
      }
    },
    experiments: {
      outputModule: true
    },
    plugins: [
      new ModuleFederationPlugin({
        library: {type: "module"},

        remotes: {
          "thirdPartyDataIntegrationUi": remoteEndpoint,
        },

        shared: share({
          "@angular/core": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "@angular/common": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "@angular/common/http": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "@angular/router": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "ngx-logger": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "primeng": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "primeng/api": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "primeflex": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "primeicons": {singleton: true, strictVersion: true, requiredVersion: 'auto'},
          "rxjs": {singleton: true, strictVersion: true, requiredVersion: 'auto'},

          ...sharedMappings.getDescriptors()
        })

      }),
      sharedMappings.getPlugin()
    ],
  };
}
