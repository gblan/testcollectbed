const preventBrowserCalls = function (req, res, proxyOptions) {
  if (req.headers.accept && req.headers.accept.indexOf('html') !== -1) {
    return '/index.html';
  }
  req.headers['X-Custom-Header'] = 'yes';
};

const PROXY_CONFIG = [
  {
    context: [
      '/collect'
    ],
    target: 'https://dev-tpdi-api.kingland-cognitive.com',
    secure: false,
    logLevel: 'debug',
    bypass: preventBrowserCalls,
    headers: {"Authorization": "Bearer " + process.env.DEV_OAUTH_TOKEN}
  }
];
module.exports = PROXY_CONFIG;
